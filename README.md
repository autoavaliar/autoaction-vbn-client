# Documento do Client para VBN

## Inspirado em DSL(Domain Specific Language) internas, para controle e solicidação fluída
Abaixo um exemplo prático de como o cliente deve se comportar

## Para tests
Subir os containers
```
make up
```

Executar testes em versões PHP 5.6 e PHP 7.1
```
make test
```

## Fase de configuração

```php
<?php

$imageClient = new ImageClientBuilder(
    new UserToken('JWT USER TOKEN')
);

// Pré configuração de bucket e uri (prod ou sandbox)

// linkGenerate is on by default
$imageRequest = $imageClient
                    ->uri('https://client-vbn-service.com/')
                    ->bucket('bucketname')

                    // Opcional: if omited is true by default
                    ->linkGenerate(true);

// request de única imagem via link
$image = $imageRequest
            ->fileName('path/to/1_1_9115636_1.jpg')
         ->get()
         ;

// Resultados
echo $image->getFileName(); // 1_1_9115636_1.jpg
echo $image->getFilePath(); // path/to
echo $image->getUrl(); // url generated here
echo $image->getCreated(); // date of creation

// Image by stream
$stream = $imageRequest

            // needed to stream
            ->linkGenerate(false)

            // must be fileName (singlefile)
            ->fileName('path/to/1_1_9115636_1.jpg')

          // only work with get (doesn't work with getList)
          ->get()
          ;

// image/png or image/jpeg
// Tell browser how to present the image type
header("Content-Type: " . $stream->getContentType());

// Print as binary to browser 1024 bytes per chunk
foreach ($stream->getGenerator(1024) as $chunk) {
    echo $chunk;
}

try {
    // ----- multiple images -----
    $imageList = $imageClient
                        ->uri('https://client-photos-uri.com/')
                        ->bucket('bucketname')
                        ->addFileName('path/to/1_1_9115636_1.jpg')
                        ->addFileName('path/to/1_1_8098444_1.jpg')
                        ->addFileName('path/to/1_1_8098444_1.jpg')
                    ->getList()
                ;

    foreach ($imageList as $image) {
        echo $image->getFileName();
        echo $image->getFilePath();
        echo $image->getUrl();
        echo $image->getCreated();
    }

    if ($imageList->hasErrors()) {
        foreach ($imageList->getErrorsIterator() as $error) {
            // get the error message
            echo $error->getMessage();
            // you can log here
        }
    }

} catch (\Exception $ex) {
    // log here server comunication error
    echo $ex->getMessage();
}

// ==== Request using prefix =========
try {
    // ----- multiple images -----
    $imageList = $imageClient
                        ->uri('https://client-photos-uri.com/')
                        ->bucket('bucketname')
                        ->prefix('path/to/1_1_9115636')
                  ->getList()
                ;

    foreach ($imageList as $image) {
        echo $image->getFileName();
        echo $image->getFilePath();
        echo $image->getUrl();
        echo $image->getCreated();
    }

} catch (\Exception $ex) {
    // log here server comunication error
    echo $ex->getMessage();
}
```
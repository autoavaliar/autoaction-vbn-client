<?php
require __DIR__ . '/../vendor/autoload.php';

use AutoAction\VBN\Responses\CollectionResponses;
use PHPUnit\Framework\TestCase;

use AutoAction\VBN\Responses\SingleResponse;
use AutoAction\VBN\ValueObject\ErrorImage;
use AutoAction\VBN\ValueObject\SuccessImage;
use GuzzleHttp\Psr7\Response;

class VbnResponseTest extends TestCase
{
    /**
     * @var Response $singleResponseStub
     */
    protected $singleResponseStub;

    /**
     * @var Response $multipleResponseStub
     */
    protected $multipleResponseStub;

    /**
     * @var Response $multipleResponseStub
     */
    protected $errorMultipleResponseStub;

    public function setUp()
    {
        $this->singleResponseStub = $this->createMock(Response::class);

        $this->singleResponseStub
                ->method('getBody')
                ->willReturn(json_encode([
                    [
                        "fileName" => "path/to/arquivo.jpg",
                        "url" => "urlextensa/teste/arquivo.jpg"
                    ]
                ]));

        $this->multipleResponseStub = $this->createMock(Response::class);
        $this->multipleResponseStub
                ->method('getBody')
                ->willReturn(json_encode([
                    [
                        "fileName" => "path/to/arquivo1.jpg",
                        "url" => "urlextensa/teste/arquivo1.jpg"
                    ],
                    [
                        "fileName" => "path/to/arquivo2.jpg",
                        "url" => "urlextensa/teste/arquivo2.jpg"
                    ],
                    [
                        "fileName" => "path/to/arquivo3.jpg",
                        "url" => "",
                        "message" => "File not found"
                    ],
                    [
                        "fileName" => "path/to/arquivo4.jpg",
                        "url" => "",
                        "message" => "Permission denied. Instance, entity or country is not allowed to get the image"
                    ]
                ]));
        
        $this->multipleResponseStub
                ->method('getStatusCode')
                ->willReturn(200);

        $this->errorMultipleResponseStub = $this->createMock(Response::class);
        $this->errorMultipleResponseStub
            ->method('getStatusCode')
            ->willReturn(500);
    }

    public function testResponseSingleGetData()
    {
        $singleResponse = new SingleResponse($this->singleResponseStub);

        $this->assertEquals('arquivo.jpg', $singleResponse->getFileName());
        $this->assertEquals('path/to', $singleResponse->getFilePath());
        $this->assertEquals('urlextensa/teste/arquivo.jpg', $singleResponse->getUrl());
    }
    
    public function testCollectionResponsesHttpStatusError()
    {
        $this->expectExceptionMessage('Error response not 200 OK');

        $collectionResponse = new CollectionResponses($this->errorMultipleResponseStub);
    }

    public function testCollectionResponsesGetData()
    {
        $collectionResponse = new CollectionResponses($this->multipleResponseStub);

        $this->assertEquals(2, count($collectionResponse));
    }

    public function testCollectionResponsesGetDataWithErrors()
    {
        $collectionResponse = new CollectionResponses($this->multipleResponseStub);

        $this->assertTrue($collectionResponse->hasErrors());
        $this->assertEquals(2, count($collectionResponse->getErrorsIterator()));
    }

    public function testCollectionResponsesGetFieldValueSuccessImage()
    {
        $collectionResponse = new CollectionResponses($this->multipleResponseStub);

        $this->assertInstanceOf(SuccessImage::class, $collectionResponse[0]);
        $this->assertEquals("arquivo1.jpg", $collectionResponse[0]->getFileName());
        $this->assertEquals("path/to", $collectionResponse[0]->getFilePath());
        $this->assertEquals("urlextensa/teste/arquivo1.jpg", $collectionResponse[0]->getUrl());
    }

    public function testCollectionResponsesGetFieldValueErrorImage()
    {
        $collectionResponse = new CollectionResponses($this->multipleResponseStub);

        $errorList = $collectionResponse->getErrorsIterator();

        $this->assertInstanceOf(ErrorImage::class, $errorList[0]);
        $this->assertEquals("arquivo3.jpg", $errorList[0]->getFileName());
        $this->assertEquals("path/to", $errorList[0]->getFilePath());
        $this->assertEquals("File not found", $errorList[0]->getMessage());

        $this->assertInstanceOf(ErrorImage::class, $errorList[1]);
        $this->assertEquals("arquivo4.jpg", $errorList[1]->getFileName());
        $this->assertEquals("path/to", $errorList[1]->getFilePath());
        $this->assertEquals("Permission denied. Instance, entity or country is not allowed to get the image", $errorList[1]->getMessage());
    }
}
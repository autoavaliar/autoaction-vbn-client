<?php
require __DIR__ . '/../vendor/autoload.php';

use PHPUnit\Framework\TestCase;

use AutoAction\VBN\ImageClientBuilder;
use AutoAction\VBN\ValueObject\UserToken;
use GuzzleHttp\Psr7\Response;

class VbnClientTest extends TestCase
{
    public function testGetRequestByOneLink()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $image = $imageClient
                    ->uri('https://mockurl.co/')
                    ->bucket('bucketname')
                    ->linkGenerate(true)
                    ->fileName('path/to/1_1_9115636_1.jpg')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://mockurl.co/',
            'bucket'            => 'bucketname',
            'prefix'            => '',
            'fileNames'         => [],
            'linkGenerate'      => 1,
            'fileName'          => 'path/to/1_1_9115636_1.jpg'
        ]);
    }

    public function testGetRequestByOneLinkWithoutLinkGenerate()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $image = $imageClient
                    ->uri('https://mockurl.co/')
                    ->bucket('bucketname')
                    ->fileName('path/to/1_1_9115636_1.jpg')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://mockurl.co/',
            'bucket'            => 'bucketname',
            'prefix'            => '',
            'fileNames'         => [],
            'linkGenerate'      => 1,
            'fileName'          => 'path/to/1_1_9115636_1.jpg'
        ]);
    }

    public function testGetRequestLinksWithPrefix()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $image = $imageClient
                    ->uri('https://mockurl.co/')
                    ->bucket('bucketname')
                    ->prefix('path/to/1_1_9115636')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://mockurl.co/',
            'bucket'            => 'bucketname',
            'prefix'            => 'path/to/1_1_9115636',
            'fileNames'         => [],
            'linkGenerate'      => 1,
            'fileName'          => ''
        ]);
    }

    public function testGetRequestByOneNotGenerateLink()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $image = $imageClient
                    ->uri('https://mockurl.co/')
                    ->bucket('b2brepo')
                    ->linkGenerate(false)
                    ->fileName('path/to/1_1_9115636_1.jpg')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://mockurl.co/',
            'bucket'            => 'b2brepo',
            'prefix'            => '',
            'fileNames'         => [],
            'linkGenerate'      => 0,
            'fileName'          => 'path/to/1_1_9115636_1.jpg'
        ]);
    }

    public function testGetRequestByMultipleLinks()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $image = $imageClient
                    ->uri('https://fakeurl.com/')
                    ->bucket('bucketmocked')
                    ->linkGenerate(true)
                    ->addFileName('path/to/1_1_9115636_1.jpg')
                    ->addFileName('path/to/1_1_8098444_1.jpg')
                    ->addFileName('path/to/1_1_1234567_1.jpg')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://fakeurl.com/',
            'bucket'            => 'bucketmocked',
            'prefix'            => '',
            'fileNames'         => [
                'path/to/1_1_9115636_1.jpg',
                'path/to/1_1_8098444_1.jpg',
                'path/to/1_1_1234567_1.jpg',
            ],
            'linkGenerate'      => 1,
            'fileName' => ''
        ]);
    }

    public function testClientDividedConfigRequest()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $imageRequest = $imageClient
                            ->uri('https://fakeurl.com/');
        
        $image = $imageRequest
                    ->bucket('bucketmocked')
                    ->linkGenerate(true)
                    ->fileName('path/to/1_1_9115636_1.jpg')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://fakeurl.com/',
            'bucket'            => 'bucketmocked',
            'prefix'            => '',
            'fileName'          => 'path/to/1_1_9115636_1.jpg',
            'fileNames'         => [],
            'linkGenerate'      => 1
        ]);
    }

    public function testClientMoreThanOneRequest()
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );
        
        $imageRequest = $imageClient
                            ->uri('https://fakeurl.com/')
                            ->bucket('bucketmocked')
                            ;
        
        $image = $imageRequest
                    ->linkGenerate(true)
                    ->fileName('path/to/1_1_9115636_1.jpg')
                 ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://fakeurl.com/',
            'prefix'            => '',
            'bucket'            => 'bucketmocked',
            'fileName'          => 'path/to/1_1_9115636_1.jpg',
            'fileNames'         => [],
            'linkGenerate'      => 1
        ]);

        // force clear = happens inside get/getList
        $imageRequest->resetFiles();

        $images = $imageRequest
                        ->linkGenerate(true)
                        ->addfileName('path/to/1_1_9115636_1.jpg')
                        ->addfileName('path/to/1_1_8098444_1.jpg')
                        ->addfileName('path/to/1_1_1234567_1.jpg')
                    ;

        $this->assertEquals($image->getState(), (object) [
            'userToken'         => '123456',
            'baseUri'           => 'https://fakeurl.com/',
            'bucket'            => 'bucketmocked',
            'prefix'            => '',
            'fileNames'         => [
                'path/to/1_1_9115636_1.jpg',
                'path/to/1_1_8098444_1.jpg',
                'path/to/1_1_1234567_1.jpg',
            ],
            'linkGenerate'      => 1,
            'fileName' => ''
        ]);
    }

    public function testClientInvalidAndIncompleteConfig()
    {
        $this->expectExceptionMessage("Configurantion incomplete");

        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );

        $imageClient
            ->uri('https://fakeurl.com/')
            ->linkGenerate(true)
            ->get()
        ;
    }

    public function dataClientProvider()
    {
        return [
            ['userToken not defined or to small', '1234', 'uri://teste.com', true, 'bucket', 'filename.jpg'],
            ['baseUri not defined or to small', '123456', '', true, 'bucket', 'filename.jpg'],
            ['bucket not defined or to small', '123456', 'uri://teste.com', false, '', 'filename.jpg'],
            ['fileName not defined or to small', '123456', 'uri://teste.com', true, 'bucket', 'fil'],
        ];
    }

    /**
     * @dataProvider dataClientProvider
     */
    public function testClientIncompleteConfig($err, $token, $uri, $link, $bucket, $fileName)
    {
        $this->expectExceptionMessage($err);

        $imageClient = new ImageClientBuilder(
            new UserToken($token)
        );

        $imageClient
            ->uri($uri)
            ->linkGenerate($link)
            ->bucket($bucket)
            ->fileName($fileName)
        ->get();
    }

    public function testClientNoFilesConfig()
    {
        $this->expectExceptionMessage("No fileName() or addFileName() specified");

        $imageClient = new ImageClientBuilder(
            new UserToken('1234567')
        );

        $imageClient
            ->uri('https://teste.com')
            ->bucket('bucketname')
        ->get();
    }

    public function dataIsContentTypeJsonProvider()
    {
        $response = new Response(200, ['Content-Type' => 'application/json'], "{}");
        return [
            [new Response(200, ['Content-Type' => 'application/json'], "{}"), true],
            [new Response(200, ['Content-Type' => 'application/json; charset=utf-8'], "{}"), true],
            [new Response(200, ['Content-Type' => 'application/json;charset=utf-8'], "{}"), true],
            [new Response(200, ['Content-Type' => 'image/jpeg'], "{}"), false],
            [new Response(200, ['Content-Type' => 'image/png'], "{}"), false],
            [new Response(200, ['Content-Type' => 'application/json   ; charset=utf-8'], "{}"), true],
            [new Response(200, [], "{}"), false],
        ];
    }

    /**
     * @dataProvider dataIsContentTypeJsonProvider
     */
    public function testCheckIsContentTypeJson($response, $result)
    {
        $imageClient = new ImageClientBuilder(
            new UserToken('123456')
        );

        if (!isset($response->getHeader('Content-Type')[0])) {
            $this->expectExceptionMessage('Content-Type not returned');
        }

        $return = $imageClient->isContentTypeJson($response);
        
        $this->assertEquals($result, $return);
    }
}
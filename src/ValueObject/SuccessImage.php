<?php

namespace AutoAction\VBN\ValueObject;

class SuccessImage
{
    protected $fileName;

    protected $url;

    protected $created;

    public function __construct($name, $url, $created)
    {
        $this->fileName = $name;
        $this->url = $url;
        $this->created = $created;
    }

    public function getFilePath()
    {
        return dirname($this->fileName);
    }

    public function getFileName()
    {
        return basename($this->fileName);
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getCreated()
    {
        return $this->created;
    }
}

<?php

namespace AutoAction\VBN\ValueObject;

class ErrorImage
{
    protected $fileName;

    protected $message;

    public function __construct($name, $message)
    {
        $this->fileName = $name;
        $this->message = $message;
    }

    public function getFileName()
    {
        return basename($this->fileName);
    }

    public function getFilePath()
    {
        return dirname($this->fileName);
    }

    public function getUrl()
    {
        return '';
    }

    public function getMessage()
    {
        return $this->message;
    }
}

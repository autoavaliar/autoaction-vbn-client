<?php

namespace AutoAction\VBN\ValueObject;

class UserToken
{
    protected $token;

    public function __construct($token)
    {
        $this->setToken($token);
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }
}

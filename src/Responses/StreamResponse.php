<?php

namespace AutoAction\VBN\Responses;

use Generator;
use GuzzleHttp\Psr7\Response;

class StreamResponse
{
    protected $response;

    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    public function getGenerator($readSize)
    {
        while ($input = $this->response->getBody()->read($readSize)) {
            yield $input;
        }
    }

    public function getContentType()
    {
        return $this->response->getHeader('Content-Type')[0];
    }
}

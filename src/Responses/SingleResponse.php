<?php

namespace AutoAction\VBN\Responses;

use Psr\Http\Message\ResponseInterface;

class SingleResponse
{
    protected $fileName;

    protected $url;

    protected $created;

    protected $filledResponse;

    public function __construct(ResponseInterface $response)
    {
        $json = json_decode($response->getBody());
        $imageData = $json[0];

        $this->filledResponse = $imageData;

        $this->fileName = $imageData->fileName;
        $this->url = $imageData->url;
    }

    public function getFileName()
    {
        return basename($this->fileName);
    }

    public function getFilePath()
    {
        return dirname($this->fileName);
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getLastResponse()
    {
        return $this->filledResponse;
    }
}

<?php

namespace AutoAction\VBN\Responses;

use ArrayIterator;
use AutoAction\VBN\ValueObject\ErrorImage;
use AutoAction\VBN\ValueObject\SuccessImage;
use Psr\Http\Message\ResponseInterface;

class CollectionResponses extends ArrayIterator
{
    protected $errorIterator;

    protected $hasErrorValue;

    public function __construct(ResponseInterface $response)
    {
        if ($response->getStatusCode() !== 200) {
            throw new \Exception("Error response not 200 OK");
        }

        $this->errorIterator = new ArrayIterator();

        $jsonParsed = json_decode($response->getBody());

        foreach ($jsonParsed as $image) {
            if (isset($image->message)) {
                $this->appendError(new ErrorImage($image->fileName, $image->message));
                $this->hasErrorValue = true;
                continue;
            }
            $this->appendSuccess(new SuccessImage($image->fileName, $image->url, $image->created));
        }
    }

    public function appendSuccess(SuccessImage $image)
    {
        $this->append($image);
    }

    public function appendError(ErrorImage $image)
    {
        $this->errorIterator->append($image);
    }

    public function hasErrors()
    {
        return $this->hasErrorValue;
    }

    public function getErrorsIterator()
    {
        return $this->errorIterator;
    }
}

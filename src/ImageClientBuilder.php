<?php

namespace AutoAction\VBN;

use AutoAction\VBN\Responses\CollectionResponses;
use AutoAction\VBN\Responses\SingleResponse;
use AutoAction\VBN\Responses\StreamResponse;
use AutoAction\VBN\ValueObject\UserToken;
use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;
use stdClass;

class ImageClientBuilder
{
    protected $state;

    public function __construct(UserToken $userToken)
    {
        $this->state = new stdClass();
        $this->state->userToken = $userToken->getToken();
        $this->state->fileNames = [];
        $this->state->fileName = '';
        $this->state->linkGenerate = 1;
        $this->state->prefix = "";
        $this->uriPath = '/';
    }

    protected function makeClient()
    {
        return new Client([
            'base_uri' => $this->state->baseUri,
            'headers' => [
                'token' => $this->state->userToken
            ]
        ]);
    }

    public function uri($uri)
    {
        $parsedUrl = parse_url($uri);
        if (isset($parsedUrl['path'])) {
            $this->uriPath = $parsedUrl['path'];
        }

        $this->state->baseUri = $uri;
        return $this;
    }

    public function bucket($bucket)
    {
        $this->state->bucket = $bucket;
        return $this;
    }

    public function linkGenerate($linkGenerate)
    {
        $this->state->linkGenerate = $linkGenerate ? 1 : 0;
        return $this;
    }

    public function addFileName($filename)
    {
        $this->state->fileNames[] = $filename;
        return $this;
    }

    public function fileName($filename)
    {
        $this->state->fileName = $filename;
        return $this;
    }

    public function prefix($prefix)
    {
        $this->state->prefix = $prefix;
        return $this;
    }

    public function isContentTypeJson(ResponseInterface $response)
    {
        $contentType = isset($response->getHeader('Content-Type')[0]) ? $response->getHeader('Content-Type')[0] : false;
        if ($contentType === false) {
            throw new \Exception("Content-Type not returned");
        }

        $first = explode(';', $contentType)[0];

        return trim($first) === 'application/json';
    }

    public function get()
    {
        $this->validateState();
        $response = $this->request([$this->state->fileName]);

        if ($this->isContentTypeJson($response)) {
            return new SingleResponse($response);
        }
        return new StreamResponse($response);
    }

    public function getList()
    {
        $this->validateState();
        return new CollectionResponses(
            $this->request($this->state->fileNames)
        );
    }

    protected function request(array $filenames)
    {
        $response = $this->makeClient()
                         ->request(
                             'POST',
                             $this->uriPath,
                             [
                                 'json' => [
                                     'bucketName' => $this->state->bucket,
                                     'linkGenerate' => $this->state->linkGenerate,
                                     'prefix' => $this->state->prefix,
                                     'fileName' => $filenames
                                 ]
                             ]
                        );

        if ($response->getStatusCode() === 200) {
            $this->resetFiles();
            return $response;
        }
    }

    public function resetState()
    {
        $this->state = new stdClass();
        return $this;
    }

    public function getState()
    {
        return $this->state;
    }

    public function resetFiles()
    {
        $this->state->fileName = '';
        $this->state->fileNames = [];
        return $this;
    }

    public function validateState()
    {
        if (count((array)$this->state) < 7) {
            throw new \Exception('Configurantion incomplete');
        }

        if (!isset($this->state->userToken) || strlen($this->state->userToken) < 5) {
            throw new \Exception('userToken not defined or to small');
        }

        if (!isset($this->state->baseUri) || strlen($this->state->baseUri) < 5) {
            throw new \Exception('baseUri not defined or to small');
        }

        if (!isset($this->state->bucket) || strlen($this->state->bucket) < 3) {
            throw new \Exception('bucket not defined or to small');
        }

        if (strlen($this->state->fileName) > 0 && strlen($this->state->fileName) < 4) {
            throw new \Exception('fileName not defined or to small');
        }

        if ($this->state->fileName === '' && count($this->state->fileNames) === 0 && $this->state->prefix === "") {
            throw new \Exception('No fileName() or addFileName() specified');
        }
    }
}

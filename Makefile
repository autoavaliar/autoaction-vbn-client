up:
	docker-compose up -d

test:
	echo "PHP 5.6 tests: \n"
	docker-compose exec -u 1001 php56 /app/vendor/bin/phpunit --colors=always /app/tests
	echo "PHP 7.0 tests: \n"
	docker-compose exec -u 1001 php70 /app/vendor/bin/phpunit --colors=always /app/tests
	echo "PHP 7.1 tests: \n"
	docker-compose exec -u 1001 php71 /app/vendor/bin/phpunit --colors=always /app/tests

testphp56:
	docker-compose exec -u 1001 php56 /app/vendor/bin/phpunit --colors=always /app/tests

testphp70:
	docker-compose exec -u 1001 php70 /app/vendor/bin/phpunit --colors=always /app/tests

testphp71:
	docker-compose exec -u 1001 php71 /app/vendor/bin/phpunit --colors=always /app/tests

codefix:
	docker-compose exec -u 1001 php71 /app/vendor/bin/phpcbf --standard=PSR12 /app/src

create server:
	docker-compose exec -u 1001 php71 php -S 0.0.0.0:80 -t /app

update tsl:
	docker-compose exec php apt-get update -yqq \
		apt-get install -y --no-install-recommends openssl \
		sed -i 's,^\(MinProtocol[ ]*=\).*,\1'TLSv1.0',g' /etc/ssl/openssl.cnf \
		sed -i 's,^\(CipherString[ ]*=\).*,\1'DEFAULT@SECLEVEL=1',g' /etc/ssl/openssl.cnf\
		rm -rf /var/lib/apt/lists/*